package edu.kit.ipd.swt1.SimpleColorReduction;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Performanzermittlung
 * 
 * @author tk
 */
public class IPDPerformanceTest {

	/** Anzahl der Messläufe. */
	private static final int NUMLOOPS = 50;

	/** Alle zu testenden Bittiefen. */
	private Integer[] depths = { 3, 6, 9, 12, 15, 18, 21, 24 };

	/** Alle zu testenden Fadenanzahlen. */
	private Integer[] threads = { 1, 2, 4, 8, 16, 32, 64, 128 };

	private static BufferedImage testImage;

	@BeforeClass
	public static void setUp() throws IOException {
		testImage = ImageIO.read(new File(
				"src/test/resources/performanceTestImage.png"));
	}

	@Test
	public void testPerformance() {
		long startTime, endTime;
		long[][] measurements = new long[threads.length][depths.length];

		for (int t = 0; t < threads.length; t++) {
			int curThreadCount = threads[t];
			ColorReduction scr = new SimpleColorReduction(curThreadCount);

			for (int d = 0; d < depths.length; d++) {
				int curBitDepth = depths[d];
				// Für mehr Messgenauigkeit :)
				for (int loop = 0; loop < NUMLOOPS; loop++) {

					WritableRaster raster = testImage.copyData(null);
					BufferedImage myImage = new BufferedImage(
							testImage.getColorModel(), raster,
							testImage.isAlphaPremultiplied(), null);

					// Start der Messung
					startTime = System.currentTimeMillis();
					scr.setDestBitDepth(curBitDepth);
					scr.setSourceImage(myImage);
					BufferedImage destImage = scr.getReducedImage();

					// Ende der Messung
					endTime = System.currentTimeMillis();
					measurements[t][d] += endTime - startTime;
				}
				measurements[t][d] *= 1000.0 / NUMLOOPS;
			}
		}
		System.out.println("Messergebnisse:");
		System.out.println(Arrays.deepToString(measurements));
	}
}
