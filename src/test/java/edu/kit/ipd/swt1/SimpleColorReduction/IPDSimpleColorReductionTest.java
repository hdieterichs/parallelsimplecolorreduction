package edu.kit.ipd.swt1.SimpleColorReduction;

/**
 * Einfacher Test, der den abstrakten Testfall ColorReductionTest implementiert.
 * Dadurch erbt er alle Testfunktionen und muss nur noch ein Objekt seiner zu
 * testenden Klasse zurueckgeben.
 * 
 * @author Christopher Guckes (christopher.guckes@student.kit.edu)
 * @version 1.0
 */
public class IPDSimpleColorReductionTest extends IPDColorReductionTest {
	@Override
	public ColorReduction getTestObject() {
		return new SimpleColorReduction();
	}

}
