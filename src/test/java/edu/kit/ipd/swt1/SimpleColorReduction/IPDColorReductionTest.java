package edu.kit.ipd.swt1.SimpleColorReduction;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

/**
 * Abstrakte Klasse für Modultests von SimpleColorReduction.
 * 
 * @author tk
 */
public abstract class IPDColorReductionTest {
	/**
	 * Hält Instanz unter Test.
	 */
	private ColorReduction testObject;

	/**
	 * Hält Testbild.
	 */
	private BufferedImage testImage;

	/**
	 * Alle zu testenden Bittiefen.
	 */
	private Integer[] depths = { 3, 6, 9, 12, 15, 18, 21, 24 };

	/**
	 * Liste der Bilder, die als Testdaten dienen.
	 */
	private ArrayList<BufferedImage> referenceImages;

	/**
	 * Erstellt Testfallumgebung: Bilder auslesen, Instanz der Klasse unter Test
	 * erzeugen.
	 * 
	 * @throws IOException
	 *             Falls eine Testbilddatei nicht gelesen werden kann.
	 */
	@Before
	public void setUp() throws IOException {
		this.testObject = this.getTestObject();
		this.testImage = ImageIO.read(new File(
				"src/test/resources/original.png"));

		this.referenceImages = new ArrayList<>();

		for (Integer depth : this.depths) {
			File imageFile = new File("src/test/resources/"
					+ this.testObject.getClass().getSimpleName() + "-" + depth
					+ "bit.png");
			this.referenceImages.add((depth / 3) - 1, ImageIO.read(imageFile));
		}
	}

	/**
	 * Erbende Klasse stellt Testinstanz bereit. (So lassen sich mit wenigen
	 * Handgriffen weitere Implementierungen der Schnittstelle ColorReduction
	 * mit den gleichen Testfällen testen.)
	 * 
	 * @return Instanz der Implementierung unter Test
	 */
	public abstract ColorReduction getTestObject();

	/**
	 * Testet, ob die Implementierung das Testbild korrekt auf alle geforderten
	 * Bittiefen reduziert.
	 */
	@Test
	public void testGetReducedImage() {
		for (Integer depth : this.depths) {
			BufferedImage result;
			try {
				this.setUp();
			} catch (IOException e) {
				fail("Problems during test setup, test images not found.");
			}
			this.testObject.setDestBitDepth(depth);
			this.testObject.setSourceImage(this.testImage);
			result = this.testObject.getReducedImage();

			assertTrue(imagesEqual(result,
					this.referenceImages.get((depth / 3 - 1))));
		}
	}

	/**
	 * Testet, ob ein eine unvollstaendige Initialisierung eine Ausnahme
	 * ausloest. In diesem Fall werden sowohl das Quellbild, als auch die
	 * gewuenschte Farbtiefe nicht gesetzt.
	 */
	@Test(expected = RuntimeException.class)
	public void testGetReducedImageException1() {
		this.testObject.getReducedImage();
	}

	/**
	 * Testet, ob ein eine unvollstaendige Initialisierung eine Ausnahme
	 * ausloest. In diesem Fall wird das Quellbild nicht gesetzt.
	 */
	@Test(expected = RuntimeException.class)
	public void testGetReducedImageException2() {
		this.testObject.setDestBitDepth(3);
		this.testObject.getReducedImage();
	}

	/**
	 * Testet, ob eine ob ein eine unvollstaendige Initialisierung eine Ausnahme
	 * ausloest. In diesem Fall wird die gewuenschte Farbtiefe nicht gesetzt.
	 */
	@Test(expected = RuntimeException.class)
	public void testGetReducedImageException3() {
		this.testObject.setSourceImage(this.testImage);
		this.testObject.getReducedImage();
	}

	/**
	 * Prueft, ob das Belegen der Initialwerte mit null Probleme erzeugt, und ob
	 * diese mit einer Exception (nicht Nullpointer-Exception) abgefangen
	 * werden.
	 */
	@Test(expected = RuntimeException.class)
	public void testGetReducedImageNull() {
		Boolean nullPointerExceptionTrown = false;

		this.testObject.setDestBitDepth(null);
		this.testObject.setSourceImage(null);
		try {
			this.testObject.getReducedImage();
		} catch (NullPointerException npe) {
			nullPointerExceptionTrown = true;
		}

		assertTrue(!nullPointerExceptionTrown);
	}

	/**
	 * Testet, ob fehlerhafte Werte in den Initialisierungsfunktionen mit einer
	 * entsprechenden Exception abgefangen werden. Hier wird die Farbtiefe auf
	 * Null gesetzt.
	 */
	@Test(expected = RuntimeException.class)
	public void testGetReducedImageInvalidInput1() {
		this.testObject.setDestBitDepth(0);
		this.testObject.setSourceImage(this.testImage);
		this.testObject.getReducedImage();
	}

	/**
	 * Testet, ob fehlerhafte Werte in den Initialisierungsfunktionen mit einer
	 * entsprechenden Exception abgefangen werden. Hier wird die Farbtiefe auf
	 * eine Zahl gesetzt, die nicht durch drei teilbar ist.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetReducedImageInvalidInput2() {
		this.checkDivByZero(1);
	}

	/**
	 * Testet, ob fehlerhafte Werte in den Initialisierungsfunktionen mit einer
	 * entsprechenden Exception abgefangen werden. Hier wird die Farbtiefe auf
	 * einen negativen Wert gesetzt (durch drei teilbar).
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetReducedImageInvalidInput3() {
		this.checkDivByZero(-3);
	}

	/**
	 * Testet, ob fehlerhafte Werte in den Initialisierungsfunktionen mit einer
	 * entsprechenden Exception abgefangen werden. Hier wird die Farbtiefe auf
	 * einen Wert ueber 24 gesetzt (durch drei teilbar).
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testGetReducedImageInvalidInput4() {
		this.checkDivByZero(27);
	}

	/**
	 * Testet, ob fehlerhafte Werte in den Initialisierungsfunktionen mit einer
	 * entsprechenden Exception abgefangen werden. Hier wird die Farbtiefe auf
	 * fuenf gesetzt.
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthNotZeroModThree() {
		testObject.setDestBitDepth(5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthTooHigh25() {
		testObject.setDestBitDepth(25);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthTooHigh27() {
		testObject.setDestBitDepth(27);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthTooLow2() {
		testObject.setDestBitDepth(2);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthTooLow1() {
		testObject.setDestBitDepth(1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthTooLow0() {
		testObject.setDestBitDepth(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testSetBitDepthNeg() {
		testObject.setDestBitDepth(-1);
	}

	/**
	 * Hilfsfunktion des Testfalls, die es ermoeglicht, zwei Bilder auf
	 * Gleichheit zu pruefen.
	 * 
	 * @param img1 Das erste zu pruefende Bild
	 * @param img2 Das zweite zu pruefende Bild
	 * @return true, falls die Bilder identisch sind, false, falls nicht
	 */
	private Boolean imagesEqual(BufferedImage img1, BufferedImage img2) {
		Boolean myReturn = true;
		if (img1.getWidth() == img2.getWidth()
				&& img1.getHeight() == img2.getHeight()) {
			for (int x = 0; x < img1.getWidth(); x++) {
				for (int y = 0; y < img1.getHeight(); y++) {
					if (img1.getRGB(x, y) != img2.getRGB(x, y)) {
						myReturn = false;
					}
				}
			}
		} else {
			myReturn = false;
		}
		return myReturn;
	}
	
	/**
	 * Hilfsfunktion, die ueberprueft, ob eine Farbtiefenkonfiguration eine
	 * Divison durch Null zur Folge hat, bzw. ob diese richtig abgefangen wird.
	 * 
	 * @param Integer depth die zu pruefende Farbtiefe
	 */
	private void checkDivByZero(Integer depth) {
		this.testObject.setDestBitDepth(depth);
		this.testObject.setSourceImage(this.testImage);

		Boolean divByZero = false;

		try {
			this.testObject.getReducedImage();
		} catch (ArithmeticException ae) {
			divByZero = true;
		}

		assertTrue(!divByZero);
	}

	/**
	 * Minimaler Test, der aus einem 4-Pixel-Bild mit verschiedenen Farben ein
	 * schwarz-weisses Bild erzeugt (Farbtiefe 3 bit). Der Testfall geht davon
	 * aus, dass sich alle Farbraumreduktionsfunktionen, die das ColorReduction-
	 * Interface implementieren, in diesem Fall gleich verhalten.
	 */
	@Test
	public void testMinimalImage() {
		// we test an artificial image
		BufferedImage testImage = new BufferedImage(2, 2,
				BufferedImage.TYPE_INT_ARGB);
		Color c = new Color(7, 9, 11, 13);
		testImage.setRGB(0, 0, c.getRGB());
		testImage.setRGB(0, 1, c.getRGB());
		c = new Color(177, 179, 171, 193);
		testImage.setRGB(1, 0, c.getRGB());
		testImage.setRGB(1, 1, c.getRGB());

		// when reducing to 3 bit, we expect:
		BufferedImage expectedImage = new BufferedImage(2, 2,
				BufferedImage.TYPE_INT_ARGB);
		c = new Color(0, 0, 0, 13);
		expectedImage.setRGB(0, 0, c.getRGB());
		expectedImage.setRGB(0, 1, c.getRGB());
		c = new Color(128, 128, 128, 193);
		expectedImage.setRGB(1, 0, c.getRGB());
		expectedImage.setRGB(1, 1, c.getRGB());

		testObject.setDestBitDepth(3);
		testObject.setSourceImage(testImage);
		BufferedImage bi = testObject.getReducedImage();

		assertTrue(imagesEqual(expectedImage, bi));
	}
        
	/**
	 * Minimaler Test, der aus einem 4-Pixel-Bild mit verschiedenen Farben ein
	 * schwarz-weisses Bild erzeugt (Farbtiefe 3 bit). Der Testfall geht davon
	 * aus, dass sich alle Farbraumreduktionsfunktionen, die das ColorReduction-
	 * Interface implementieren, in diesem Fall gleich verhalten.
	 */
	@Test
	public void testMinimalImage2() {
		// we test an artificial image
		BufferedImage testImage = new BufferedImage(500, 500,
				BufferedImage.TYPE_INT_ARGB);
		Color c = new Color(7, 9, 11, 13);
		testImage.setRGB(0, 0, c.getRGB());
		testImage.setRGB(0, 1, c.getRGB());
		c = new Color(177, 179, 171, 193);
		testImage.setRGB(1, 0, c.getRGB());
		testImage.setRGB(1, 1, c.getRGB());
		testImage.setRGB(499, 0, c.getRGB());
		testImage.setRGB(499, 1, c.getRGB());
		testImage.setRGB(0, 499, c.getRGB());
		testImage.setRGB(1, 499, c.getRGB());
                
		// when reducing to 3 bit, we expect:
		BufferedImage expectedImage = new BufferedImage(500, 500,
				BufferedImage.TYPE_INT_ARGB);
		c = new Color(0, 0, 0, 13);
		expectedImage.setRGB(0, 0, c.getRGB());
		expectedImage.setRGB(0, 1, c.getRGB());
		c = new Color(128, 128, 128, 193);
		expectedImage.setRGB(1, 0, c.getRGB());
		expectedImage.setRGB(1, 1, c.getRGB());
		expectedImage.setRGB(499, 0, c.getRGB());
		expectedImage.setRGB(499, 1, c.getRGB());
		expectedImage.setRGB(0, 499, c.getRGB());
		expectedImage.setRGB(1, 499, c.getRGB());
                
		testObject.setDestBitDepth(3);
		testObject.setSourceImage(testImage);
		BufferedImage bi = testObject.getReducedImage();

		assertTrue(imagesEqual(expectedImage, bi));
	}
}
