package edu.kit.ipd.swt1.SimpleColorReduction;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.PixelInterleavedSampleModel;
import java.awt.image.SinglePixelPackedSampleModel;
import java.awt.image.WritableRaster;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Color reduction class.
 *
 * Note: Java is not made for this kind of performance optimization.
 * 
 * @author Henning Dieterichs
 */
public class SimpleColorReduction implements ColorReduction {

    private BufferedImage sourceImage;
    private BufferedImage destImage;
    private Integer destBitDepth = 0;
    private boolean copySourceImage = false;
    private final int threadCount;
    
    public SimpleColorReduction(int threadCount) {
        this.threadCount = threadCount;
    }
    
    public SimpleColorReduction() {
        this.threadCount = Runtime.getRuntime().availableProcessors();
    }
    
    @Override
    public void setSourceImage(BufferedImage sourceImage) {
        this.sourceImage = sourceImage;
        this.destImage = null;
    }

    @Override
    public void setDestBitDepth(Integer destBitDepth) {
        if (destBitDepth >= 3 && destBitDepth <= 24
                && destBitDepth % 3 == 0) {
            this.destBitDepth = destBitDepth;
            this.destImage = null;
        } else {
            throw new IllegalArgumentException(
                    "Farbtiefe muss zwischen 3 und 24 liegen und durch 3 teilbar sein!");
        }
    }

    @Override
    public BufferedImage getReducedImage() {
        if (sourceImage == null || destBitDepth == 0) {
            throw new IllegalStateException(
                    "Vorher Ausgangsbild und gewÃ¼nschte Bittiefe setzen.");
        }
        if (destImage == null) {
            destImage = reduceColors(sourceImage);
        }
        return destImage;
    }

    /**
     * Sets whether to copy the source image.
     * @param copySourceImage true, if the source image should be copied, 
     * false if it should be reused.
     */
    public void setCopySourceImage(boolean copySourceImage) {
        this.copySourceImage = copySourceImage;
    }

    private int getThreadCount() {
        return threadCount;
    }

    private BufferedImage reduceColors(BufferedImage imageToReduce) {
        WritableRaster raster;
        BufferedImage result;

        if (copySourceImage) {
            raster = imageToReduce.copyData(null);
            result = new BufferedImage(imageToReduce.getColorModel(), raster,
                    imageToReduce.isAlphaPremultiplied(), null);
        } else {
            result = imageToReduce;
            raster = result.getRaster();
        }

        ExecutorService pool = Executors.newFixedThreadPool(getThreadCount());
        
        createReduceColorWorkers(raster, pool);

        try {
            pool.shutdown();
            pool.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        return result;
    }

    private int getInversedSingleColorBitMask() {
        int bitDepthPerChannel = destBitDepth / 3;
        //sets the first 8 - bitDepthPerChannel bits to 1.
        return (1 << (8 - bitDepthPerChannel)) - 1;
    }
    
    private void createReduceColorWorkers(WritableRaster raster, ExecutorService pool) {

        DataBuffer buffer = raster.getDataBuffer();

        if (buffer instanceof DataBufferInt
                && raster.getSampleModel() instanceof SinglePixelPackedSampleModel) {
            createWorkersForDataBufferIntAndSinglePixelPackedSampleModel(
                    (DataBufferInt) buffer, raster.getHeight() * raster.getWidth(), pool);
            return;
        }
        else if (buffer instanceof DataBufferByte
                && raster.getSampleModel() instanceof PixelInterleavedSampleModel) {
            //*3, because each color is stored in 3 bytes.
            createWorkersForDataBufferByteAndPixelInterleavedSampleModel(
                    (DataBufferByte) buffer, raster.getHeight() * raster.getWidth() * 3, pool);
            return;
        }

        createFallbackReduceColorWorkers(raster, pool);
    }
    
   /**
     * Reduces the image by using a DataBufferInt.
     * Assumes that each color is stored in 1 int.
     */
    private void createWorkersForDataBufferIntAndSinglePixelPackedSampleModel(
            DataBufferInt buffer, int count, ExecutorService pool) {

        int[] data = buffer.getData();

        //31....24 23....16 15....08 07....00
        // Alpha     Red     Green     Blue
        //Example bit masks to reduce the bit depth:
        //11111111 11111000 11111000 11111000 (Bit depth 5)
        //11111111 11000000 11000000 11000000 (Bit depth 2)

        int singleColorChannelBitMask = getInversedSingleColorBitMask();
        int colorBitMask = ~(singleColorChannelBitMask << 16
                | singleColorChannelBitMask << 8 | singleColorChannelBitMask);

        int threadCount = Math.min(getThreadCount(), count);
        int countPerThread = count / threadCount;

        for (int i = 0; i < threadCount; i++) {
            int overtime = (i == threadCount - 1) ? (count % threadCount) : 0;
            pool.submit(new ApplyBitmaskToIntArrayRunnable(data, colorBitMask,
                    buffer.getOffset() + i * countPerThread,
                    countPerThread + overtime));
        }
    }


    /**
     * Reduces the image by using a DataBufferByte.
     * Assumes that each color takes 3 bytes.
     */
    private void createWorkersForDataBufferByteAndPixelInterleavedSampleModel(
            DataBufferByte buffer, int count, ExecutorService pool) {
        
        byte[] data = buffer.getData();
        byte colorBitMask = (byte)~getInversedSingleColorBitMask();

        int threadCount = Math.min(getThreadCount(), count);
        int countPerThread = count / threadCount;

        for (int i = 0; i < threadCount; i++) {
            int overtime = (i == threadCount - 1) ? (count % threadCount) : 0;
            pool.submit(new ApplyBitmaskToByteArrayRunnable(data, colorBitMask,
                    i * countPerThread, countPerThread + overtime));
        }
    }
    
    /**
     * Fallback workers which ensure that even images can be reduced which use a
     * different sample model or data buffer than SinglePixelPackedSampleModel
     * and DataBufferInt.
     */
    private void createFallbackReduceColorWorkers(WritableRaster raster, ExecutorService pool) {

        int height = raster.getHeight();
        int threadCount = Math.min(getThreadCount(), height);
        int heightPerThread = height / threadCount;

        for (int i = 0; i < threadCount; i++) {
            int overtime = (i == threadCount - 1) ? (height % threadCount) : 0;
            pool.submit(new ApplyBitmaskToRasterRunnable(raster, 
                    ~getInversedSingleColorBitMask(),
                    i * heightPerThread, heightPerThread + overtime));
        }
    }

    
    static class ApplyBitmaskToIntArrayRunnable implements Runnable {

        private final int[] data;
        private final int bitmask;
        private final int start;
        private final int count;

        public ApplyBitmaskToIntArrayRunnable(int[] data, int bitmask, int start, int count) {
            this.data = data;
            this.bitmask = bitmask;
            this.start = start;
            this.count = count;
        }

        @Override
        public void run() {
            //end is exclusive
            int end = start + count;

            //copy fields to local variables to improve performance
            int[] localData = this.data;
            int localBitmask = this.bitmask;

            for (int pos = start; pos != end; ++pos) {
                //apply the bitmask to the pixel.
                localData[pos] = localData[pos] & localBitmask;
            }
        }
    }

    static class ApplyBitmaskToByteArrayRunnable implements Runnable {

        private final byte[] data;
        private final int start;
        private final int count;
        private final byte bitmask;
        
        public ApplyBitmaskToByteArrayRunnable(byte[] data, byte bitmask, 
                int start, int count) {
            this.data = data;
            this.start = start;
            this.count = count;
            this.bitmask = bitmask;
        }

        @Override
        public void run() {
            //end is exclusive
            int end = start + count;

            //copy fields to local variables to improve performance
            byte[] localData = this.data;
            byte localBitmask = bitmask;
           
            for (int pos = start; pos != end; ++pos) {
                this.data[pos] = (byte)(this.data[pos] & this.bitmask);
            }
        }
    }
    
    static class ApplyBitmaskToRasterRunnable implements Runnable {

        private final WritableRaster raster;
        private final int bitmask;
        private final int heightStart;
        private final int heightCount;

        public ApplyBitmaskToRasterRunnable(WritableRaster raster, int bitmask,
                int heightStart, int heightCount) {
            this.raster = raster;
            this.bitmask = bitmask;
            this.heightStart = heightStart;
            this.heightCount = heightCount;
        }

        @Override
        public void run() {
            int width = raster.getWidth();
            //heightEnd is exclusive
            int heightEnd = heightStart + heightCount;

            int[] pixels = new int[4];
            for (int y = heightStart; y != heightEnd; ++y) {
                for (int x = 0; x < width; ++x) {
                    //apply the bitmask to red, green and blue
                    raster.getPixel(x, y, pixels);
                    pixels[0] = pixels[0] & bitmask;
                    pixels[1] = pixels[1] & bitmask;
                    pixels[2] = pixels[2] & bitmask;
                    raster.setPixel(x, y, pixels);
                }
            }
        }
    }
}
