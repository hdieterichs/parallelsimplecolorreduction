package edu.kit.ipd.swt1.SimpleColorReduction;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Hauptklasse der Kommandozeilenschnittstelle zu
 * kit.edu.ipd.swt1.SimpleColorReduction
 * 
 * @author tk
 * 
 */
final class Main {
	/**
	 * Privater Konstruktor verhindert Instanziierung
	 */
	private Main() {
	}

	/**
	 * Main-Methode: Liest Kommandozeilenargumente ein und ruft
	 * kit.edu.ipd.swt1.SimpleColorReduction entsprechend auf.
	 * 
	 * @param args
	 *            Kommandozeilenargumente vom Benutzer
	 */
	public static void main(String[] args) {
            
		CommandLine cmd = doCommandLineParsing(args);
		BufferedImage image = null;
		try {
			File sourceFile = new File(cmd.getOptionValue("s"));
			image = ImageIO.read(sourceFile);
		} catch (IOException myException) {
			System.out.println("Quelldatei konnte nicht gelesen werden: ");
			myException.printStackTrace();
		}

		if (image == null) {
			System.out.println("Bilddatei noch nicht erfolgreich eingelesen.");
			return;
		}

		ColorReduction scr = new SimpleColorReduction();
		scr.setSourceImage(image);
		scr.setDestBitDepth(Integer.parseInt(cmd.getOptionValue("c")));
		BufferedImage destImage = scr.getReducedImage();

		try {
			File destFile = new File(cmd.getOptionValue("d"));
			ImageIO.write(destImage, "png", destFile);
		} catch (IOException myException) {
			System.out.println("Zieldatei konnte nicht geschrieben werden: ");
			myException.printStackTrace();
		}
	}

	/**
	 * Prüft Kommandozeilenargumente auf Gültigkeit
	 * 
	 * @param args
	 *            Kommandozeilenargumente vom Benutzer
	 * @return Bei Erfolg: CommandLine-Objekt mit den Werten der
	 */
	static CommandLine doCommandLineParsing(String[] args) {
		/*
		 * Lese Kommandozeilenargumente ein
		 */
		Option sourceFileOpt = new Option("s", "sourceFile", true,
				"Die Ausgangsdatei");
		sourceFileOpt.setRequired(true);
		Option destFileOpt = new Option("d", "destFile", true, "Die Zieldatei");
		destFileOpt.setRequired(true);
		Option destColorDepth = new Option("c", "colorDepth", true,
				"Die Farbtiefe, auf die reduziert werden soll, in Bits");
		destColorDepth.setRequired(true);
		destColorDepth.setType(Integer.class);
		Options options = new Options();
		options.addOption(sourceFileOpt);
		options.addOption(destFileOpt);
		options.addOption(destColorDepth);

		// create the parser
		CommandLineParser parser = new BasicParser();
		CommandLine line = null;
		try {
			// parse the command line arguments
			line = parser.parse(options, args);
		} catch (ParseException exception) {
			// oops, something went wrong
			System.err
					.println("Mit den Kommandozeilenargumenten stimmt etwas nicht: "
							+ exception.getMessage());
		}
		return line;
	}
}
